# Reactive Form Search

Angular 4 application using Redux.

Redux support provided with ngrx and effects.

Also using Push for change detection for fast efficient component updates.

![Image]("https://bytebucket.org/rajinder_yadav/reactive-search/raw/c9f0f25747df9ca8c128365081e45b97f92a4efe/reactive-search.gif")